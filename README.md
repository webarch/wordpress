# Webarchitects WordPress Ansible Role

[![pipeline status](https://git.coop/webarch/wordpress/badges/master/pipeline.svg)](https://git.coop/webarch/wordpress/-/commits/master)

An Ansible role to install, configure and upgrade [WordPress](https://www.wordpress.org/) on Debian, the [WP-CLI](https://git.coop/webarch/wpcli) role can be used to install and update the [WordPress command line interface](https://wp-cli.org/) that this role depends on.

Note that this role doesn't configure the server itself, those tasks are done using other roles &mdash; this role has been designed to be used with the [users role](https://git.coop/webarch/users), the [fail2ban role](https://git.coop/webarch/fail2ban) and the [Matomo role](https://git.coop/webarch/matomo), see the [wsh dev server repo](https://git.coop/webarch/wsh) for a full example including Apache, chrooted PHP-FPM, chrooted SSH etc, however this role should also work without these other roles, but this hasn't been tested.

See the [defaults](defaults/main.yml) for the default options, and the [checks](tasks/checks.yml) for the variables that need to be set, please use a [release tag](https://git.coop/webarch/wordpress/-/releases) in your `requirements.yml` file if you use this role.

## WP-CLI Packages

The `wordpress_packages` array is is designed to match the output of `wp package list --format=yaml` with the addition of a `status` option which can be set to `present` or `absent`.

The `wordpress_install_packages` array contains the packages to configure when WordPress is initially installed.

## WordPress Plugins

The `wordpress_plugins` array is designed to match the output of `wp plugins list --format=yaml` with the addition of a `absent` option for the status to enable plugins to be deleted.

The `wordpress_install_plugins` array contains the plugins to configure when WordPress is initially installed.

## WordPress Config

The `wordpress_config` array is designed to match the output of `wp config list --format=yaml` with the addition of a `status` option which can be set to `present` or `absent`.

The `wordpress_install_config` array contains the config to configure when WordPress is initially installed.

## WordPress Options

The `wordpress_options` array is designed to match the output of `wp options list --format=yaml` with the addition of a `status` option which can be set to `present` or `absent`.

The `wordpress_install_options` array contains the options to configure when WordPress is initially installed.

## WordPress Matomo Plugin

The automatic configuration of the `wp-piwik` plugin requires a Matomo URL, username and password and these are used to get a token authentication string to configure the plugin.

## Repository

The primary URL of this repo is [`https://git.coop/webarch/wordpress`](https://git.coop/webarch/wordpress) however it is also [mirrored to GitHub](https://github.com/webarch-coop/ansible-role-wordpress) and [available via Ansible Galaxy](https://galaxy.ansible.com/chriscroome/wordpress).

If you use this role please use a tagged release, see [the release notes](https://git.coop/webarch/wordpress/-/releases).

## Copyright

Copyright 2019-2025 Chris Croome, &lt;[chris@webarchitects.co.uk](mailto:chris@webarchitects.co.uk)&gt;.

This role is released under [the same terms as Ansible itself](https://github.com/ansible/ansible/blob/devel/COPYING), the [GNU GPLv3](LICENSE).
